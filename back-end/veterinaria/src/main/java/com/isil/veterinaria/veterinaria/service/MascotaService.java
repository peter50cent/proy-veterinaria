package com.isil.veterinaria.veterinaria.service;

import com.isil.veterinaria.veterinaria.model.Mascota;
import com.isil.veterinaria.veterinaria.repository.MascotaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class MascotaService  {

    @Autowired
    MascotaRepository mascotaRepository;
    @Transactional
    public Mascota create(Mascota mascota){
        return mascotaRepository.save(mascota);
    }


    public List<Mascota> findAll(){
        return mascotaRepository.findAll();
    }
}
