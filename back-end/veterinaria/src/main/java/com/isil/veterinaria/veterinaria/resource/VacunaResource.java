package com.isil.veterinaria.veterinaria.resource;


import com.isil.veterinaria.veterinaria.model.Vacuna;
import com.isil.veterinaria.veterinaria.service.VacunaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class VacunaResource {

    @Autowired
    private VacunaService vacunaService;

    @PostMapping("/vacuna")
    public ResponseEntity create(@RequestBody Vacuna vacuna){

        vacunaService.create(vacuna);

        return new ResponseEntity(vacuna, HttpStatus.CREATED);
    }
}
