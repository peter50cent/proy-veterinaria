package com.isil.veterinaria.veterinaria.repository;

import com.isil.veterinaria.veterinaria.model.Mascota;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MascotaRepository extends JpaRepository<Mascota,Long> {
}
