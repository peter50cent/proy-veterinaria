package com.isil.veterinaria.veterinaria.resource;

import com.isil.veterinaria.veterinaria.VeterinariaApplication;
import com.isil.veterinaria.veterinaria.model.Cliente;
import com.isil.veterinaria.veterinaria.model.Mascota;
import com.isil.veterinaria.veterinaria.service.ClienteService;
import com.isil.veterinaria.veterinaria.service.MascotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class ClienteResource  {

    @Autowired
    private ClienteService clienteService;
    @Autowired
    private MascotaService mascotaService;

    @GetMapping("/cliente")
    public ResponseEntity findAll(){
        return new ResponseEntity(clienteService.findAll(),
                HttpStatus.OK);
    }

    @PostMapping("/cliente")
    public ResponseEntity create(@RequestBody Cliente cliente){

        clienteService.create(cliente);

        return new ResponseEntity(cliente, HttpStatus.CREATED);
    }

}
