package com.isil.veterinaria.veterinaria.repository;

import com.isil.veterinaria.veterinaria.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente,Long> {
}
