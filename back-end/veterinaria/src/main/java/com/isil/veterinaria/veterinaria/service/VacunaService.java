package com.isil.veterinaria.veterinaria.service;

import com.isil.veterinaria.veterinaria.model.Vacuna;
import com.isil.veterinaria.veterinaria.repository.VacunaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VacunaService {

    @Autowired
    VacunaRepository vacunaRepository;
    @Transactional
    public Vacuna create(Vacuna vacuna){
        return vacunaRepository.save(vacuna);
    }


    public List<Vacuna> findAll(){
        return vacunaRepository.findAll();
    }
}
